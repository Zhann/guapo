package ssh

import (
	"io"
	"strings"

	"go.sorcix.com/guapo/transport"
	"golang.org/x/crypto/ssh"
)

// Check interface implementation at compile time.
var _ transport.Executer = (*SecureShell)(nil)

func mergeCommand(command string, arg ...string) string {
	if len(arg) < 1 {
		return command
	}
	return command + " " + strings.Join(arg, " ")
}

// Execute runs a command on the remote host.
func (s *SecureShell) Execute(command string, arg ...string) (err error) {
	var session *ssh.Session
	command = mergeCommand(command, arg...)
	if session, err = s.conn.NewSession(); err != nil {
		return
	}
	return session.Run(command)
}

// ExecuteOutput runs a command on the remote host and returns its standard output.
func (s *SecureShell) ExecuteOutput(command string, arg ...string) (output []byte, err error) {
	var session *ssh.Session
	command = mergeCommand(command, arg...)
	if session, err = s.conn.NewSession(); err != nil {
		return
	}
	return session.Output(command)
}

// ExecuteCombinedOutput runs a command on the remote host and returns its combined standard output and standard error.
func (s *SecureShell) ExecuteCombinedOutput(command string, arg ...string) (output []byte, err error) {
	var session *ssh.Session
	command = mergeCommand(command, arg...)
	if session, err = s.conn.NewSession(); err != nil {
		return
	}
	return session.CombinedOutput(command)
}

// ExecutePipe runs a command on the remote end and pipes its stdin and stdout.
func (s *SecureShell) ExecuteStream(stdin io.Reader, stdout io.Writer, stderr io.Writer, command string, arg ...string) (err error) {
	var session *ssh.Session
	command = mergeCommand(command, arg...)

	if session, err = s.conn.NewSession(); err != nil {
		return err
	}
	session.Stdin = stdin
	session.Stdout = stdout
	session.Stderr = stderr

	return session.Run(command)
}
