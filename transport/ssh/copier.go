package ssh

import (
	"io"

	"go.sorcix.com/guapo/transport"
)

// Check interface implementation at compile time.
var _ transport.Copier = (*SecureShell)(nil)

// CopyTo writes data to given file on the remote end.
func (s *SecureShell) CopyTo(data io.Reader, path string) (err error) {
	return nil
}

// CopyFrom reads data from given file on the remote end.
func (s *SecureShell) CopyFrom(data io.Writer, path string) (err error) {
	return nil
}

// Mkdir creates a directory on the remote end.
func (s *SecureShell) Mkdir(path string) (err error) {
	return s.Execute("mkdir", "-p", path)
}
